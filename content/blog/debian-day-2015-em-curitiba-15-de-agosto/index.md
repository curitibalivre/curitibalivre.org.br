---
kind: article
created_at: 2015-08-11
title: Debian Day 2015 em Curitiba 15 de agosto  
image_file: banner-debianday-2015.png
image_alt: Debian Day 2015
author: Paulo Henrique de Lima Santana
short_description: |
  Neste sábado (15/08) acontecerá no Debian Day 2015 em Curitiba.
---

Neste sábado (15/08) acontecerá no Debian Day 2015 em Curitiba.

O evento é totalmente gratuito e acontecerá das 13:00h às 18:00h no Auditório e a sala 04 do [SEPT - UFPR](http://www.sept.ufpr.br/) que fica localizado na Rua Doutor Alcides Vieira Arcoverde, 1225 - Jardim das América.

Estamos programando um sprint de tradução do [site do Debian](http://www.debian.org/), algumas palestras e um encontro para [assinatura de chaves GPG](http://eriberto.pro.br/wiki/index.php?title=Como_preparar-se_para_uma_festa_de_assinatura_de_chaves_GPG).

Mais informações: <http://softwarelivre.org/debianday2015-curitiba>
