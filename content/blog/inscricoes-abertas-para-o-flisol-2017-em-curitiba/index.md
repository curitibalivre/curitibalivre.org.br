---
kind: article
created_at: 2017-03-31
title: Inscrições abertas para o FLISOL 2017 em Curitiba
image_file: banner-divulgacao-flisol-2017.png
image_alt: Flisol 2017
author: Paulo Henrique de Lima Santana
short_description: |
  Já estão abertas as inscrições para o FLISOL 2017 em Curitiba que acontecerá no dia 8 de abril no SEPT UFPR:
---

Já estão abertas as inscrições para o FLISOL 2017 em Curitiba que acontecerá no dia 8 de abril no SEPT UFPR:

<http://curitiba.flisol.org.br>

![Banner FLISOL 2017](/images/divulgacao-flisol-2017.png =600x)

