---
kind: article
created_at: 2015-09-25
title: Semana Acadêmica da Opet e 3º MeetUp WordPress Curitiba 
image_file: banner-opet-wordpress.png
image_alt: Opet e WordPress
author: Paulo Henrique de Lima Santana
short_description: |
  No dia 03 de outubro de 2015 a Comunidade Curitiba Livre realizou um encontro na Aldeia Coworking para celebrar os 30 anos da Free Software Foudation. A FSF convocou as comunidades locais de software livre em todo o mundo para organizarem festas nas suas cidades e a Comunidade Curitiba Livre não poderia ficar de fora dessa.
---

Na próxima semana acontecem as atividades abaixo:

### Semana Acadêmica da Opet

28 de setembro (segunda-feira) à 03 de outubro (sábado).

Organizada com palestras, oficinas, workshop, atividades culturais, GAME JAM (sábado) e outras atividades. Na programação tem atividades com pessoal de software livre como Vitório Furusho (O poder do LibreOffice), Leonardo Saraiva (Ruby), e Márcio J Vieira (Pentaho).

O evento será gratuito e aberto! Confiram a programação, cadastrem-se e participem.

<http://semanaopet.com.br>

<http://semanaopet.com.br/php/programacao.php>

### 3º MeetUp WordPress Curitiba | Agora aos sábados!

03 de outubro (sábado).

Das 9:00h às 12:00h.

Mais informações:

<http://www.meetup.com/pt/wpcuritiba/events/225444439>

![Encontro WordPress](/images/banner-3o-meetup-wordpress.jpg)
