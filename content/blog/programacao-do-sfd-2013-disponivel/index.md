---
kind: article
created_at: 2013-08-30
title: Programação do SFD 2013 disponível  
image_file: banner-sfd-2013.png
image_alt: SFD
author: Paulo Henrique de Lima Santana
short_description: |
 A Comunidade Curitiba Livre informa que já está disponível no site do SFD - Software Freedom Day, a grade com as palestras e oficinas: 
---

A Comunidade Curitiba Livre informa que já está disponível no site do
SFD - Software Freedom Day, a grade com as palestras e oficinas:

<http://softwarelivre.org/sfd2013-curitiba/programacao>

Este ano o SFD acontecerá no dia 21 de setembro (sábado) das 10:00h às
17:00 no campus da FESP - Faculdade de Educação Superior do Paraná.

O Software Freedom Day ou em português, Dia da Liberdade do Software é
uma celebração mundial do Software Livre e de Código Aberto (SL/CA). O
objetivo com essa festa é mostrar ao público em todo o mundo os
benefícios de usar SL/CA de alta qualidade na educação, no governo, em
casa, e nos negócios, ou seja, em todos os lugares!

A inscrição é totalmente gratuita e pode ser feito no site do evento:

<http://softwarelivre.org/sfd2013-curitiba/inscricao>
