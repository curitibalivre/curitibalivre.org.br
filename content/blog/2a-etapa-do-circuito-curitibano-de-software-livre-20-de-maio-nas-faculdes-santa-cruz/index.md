---
kind: article
created_at: 2015-05-18
title: 2ª etapa do Circuito Curitibano de Software Livre - 20 de maio nas Faculdes Santa Cruz 
image_file: banner-circuito-2-santacruz.png
image_alt: Circuito 2 Santa Cruz
author: Paulo Henrique de Lima Santana
short_description: |
  A 2ª etapa do Circuito Curitibano de Software Livre acontecerá nas Faculdades Santa Cruz - Unidade Bonat, no dia 20 de maio de 2015 das 19:00h às 22h30min.
---

A **2ª etapa do Circuito Curitibano de Software Livre** acontecerá nas [Faculdades Santa Cruz](http://www.santacruz.br/) - Unidade Bonat, no dia 20 de maio de 2015 das 19:00h às 22h30min.

Serão 5 palestras apresentadas por membros da comunidade de Software Livre de Curitiba sobre os conceitos de [Software Livre](http://www.gnu.org/) e a Comunidade Curitiba Livre, a distribuição [Debian GNU/Linux](http://www.debian.org/), a suíte para escritórios [LibreOffice](https://pt-br.libreoffice.org/), a linguagem de programação [PHP](http://php.net/) e negócios em FLOSS (Free/Libre/Open Source Software).

O Circuito Curitibano de Software Livre é totalmente gratuito e nesta 2ª etapa é exclusivo para estudantes, professores e funcionários das Faculdades Santa Cruz.

Aqueles que quiserem participar do sorteio dos brindes, deverão preencher o formulário de inscrição e estar presentes no local no momento do encerramento do evento. Nesta etapa, contamos com o apoio da [Editora Novatec](http://novatec.com.br/) que doou um exemplo do livro [Shell Script Profissional](http://www.novatec.com.br/livros/shellscript/) e do Nic.br que doou um roteador wi-fi do projeto SIMETBox para sorteio. Também teremos algumas camisetas e outros brindes.

Para saber mais sobre o projeto Circuito Curitibano de Software Livre leia a nossa apresentação e se tiver interesse em levar o evento para a sua Faculdade/Universidade, entre em contato conosco através do email: <contato@curitibalivre.org.br>

![Circuito 2 Santa Cruz](/images/logos-circuito-2.png)

