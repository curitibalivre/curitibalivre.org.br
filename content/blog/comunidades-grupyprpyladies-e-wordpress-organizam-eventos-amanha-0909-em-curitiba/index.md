---
kind: article
created_at: 2015-09-08
title: Comunidades GruPyPR PyLadies e WordPress organizam eventos amanhã 09/09 em Curitiba  
image_file: banner-pyladies-wordpress.png
image_alt: PyLadies e WordPress
author: Paulo Henrique de Lima Santana
short_description: |
  Duas comunidades locais de Curitiba estão organizando encontros amanhã (09/09/15) na Aldeia Coworking.
---

Duas comunidades locais de Curitiba estão organizando encontros amanhã (09/09/15) na [Aldeia Coworking](http://aldeiaco.com.br).

**1) Encontro GruPyPR e PyLadies no Aldeia Coworking**

A partir das 18:00h.

Atividades propostas:

* Organização sobre o workshop Django girls.
* Camisetas, adesivos e canecas.
* Falar sobre participação no [FTSL](http://ftsl.org.br)
* Falar sobre participação no [Software Freedom Day](http://softwarelivre.org/sfd2015-curitiba)
* Revisar e criar as tarefas criadas pelo grupo para app do [cwbmess](https://github.com/GruPyPR/cwbmess).
* Continuar a desenvolver o [findresistor](https://github.com/GruPyPR/findresistor)
* Migração da tradução do Django do repositório do django-brasil para o transifex.
* Criar um projeto para atualizar o site do django-brasil.
* Como tornar os projetos do grupo assíncronos e facilitar a participação de quem não consegue comparecer nos evento

Mais informações:

<http://www.meetup.com/pt/GruPy-PR/events/225094006>

**2) Mais um MeetUp Curitiba para nossa alegria! (WordPress)**

A partir das 19:00h.

Mais informações:

<http://www.meetup.com/pt/wpcuritiba/events/224947433>

**Local dos dois eventos: Aldeia Coworking - Rua Marechal Deodoro, 262 - 1º andar - Galeria Suissa.**
 

