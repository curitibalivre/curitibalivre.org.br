---
kind: article
created_at: 2016-05-25
title: Proposta para a área das comunidades no FISL17
image_file: banner-fisl17.png
image_alt: FISL17
author: Paulo Henrique de Lima Santana
short_description: |
  Nome da comunidade/projeto/entidade/grupo: Comunidade Curitiba Livre.
---

**Nome da comunidade/projeto/entidade/grupo:**

Comunidade Curitiba Livre.

**Estimativa do número pessoas movimentando as atividades no espaço do grupo na Área das Comunidades:**

4 pessoas.

**Conteúdos/informações que demonstram contribuições do grupo em questão:**

Organização de eventos de software livre em Curitiba nos últimos anos como: FLISOL, Software Freedom Day, Debian Day, Education Freedom Day, Document Freedom Day.

**Proposta de atividades/ações no espaço da Área das Comunidade:**

Distribuição de panfletos sobre software livre, adesivos da comunidade, sorteio de camisetas da comunidade.

**Como contribuirão com o install fest, ou seja, que projetos de software livre vinculados à sua comunidade eles podem ajudar a instalar:**

Instalação de Debian GNU/Linux.

**Contribuição para atividades que ajudem a movimentar o FISL, ou seja, integração com os visistantes:**

Conversar com o pessoal para incentivar a criação de grupos locais de software livre que possam reunir pessoas de vários outros grupos para principalmente organizar eventos como FLISOL e Software Freedom Day.

**Lightning talks (18 minutos) que o grupo poderá promover na Área das Comunidades:**

* Ações da Comunidade Curiitba Livre para estimular o Software Livre - Daniel Lenharo de Souza
* Como o Software Livre está presente no nosso dia a dia - Leonardo Rodrigues Pereira

**Deixar claro como convidarão as pessoas:**

As pessoas serão convidadas por meio de postagens na [lista de discussão](http://listas.softwarelivre.org/cgi-bin/mailman/listinfo/curitibalivre) do grupo, e nos perfis das rede sociais como [twitter](https://twitter.com/curitibalivre) e (https://diaspora.softwarelivre.org/u/curitibalivre)[Diaspora].

**Links para as páginas e listas principais do projeto em que a comunidade/grupo faz parte, bem com a(s) lista(s) do próprio grupo:**

* [Página](http://www.curitibalivre.org.br)
* [Lista](http://listas.softwarelivre.org/cgi-bin/mailman/listinfo/curitibalivre)

**Informar se enviou uma proposta de Encontro Comunitário no FISL (via a chamada de trabalhos):**

Não foi enviado.

**Indicar de 2 a 4 coordenadores do grupo na Área das Comunidades (nome e e-mail de cada um):**

* Leonardo Rodrigues Pereira - <leontecnicalonline@gmail.com>
* Cleber de Oliveira Campos Barco Ianes - <ianes.cleber@gmail.com>
* Daniel Lenharo de Souza - <daniell@softwarelivre.org>
* Claudia Archer - <claudiarcher@gmail.com>

**Informar outras atividades: hackathon, workshop, install-party, festa de lançamento, aniversário da comunidade, mesa redonda, URC etc.**

Por enquando não foi planejado.

