---
kind: article
created_at: 2013-12-07
title: Foto da reunião de 07/12/13   
image_file: banner-foto-encontro.png
image_alt: Foto
author: Paulo Henrique de Lima Santana
short_description: |
 Publicamos uma foto do encontro realizado no dia 07 de dezembro na Garagem Hacker.
---

Publicamos uma foto do encontro realizado no dia 07 de dezembro na Garagem Hacker.


![Foto](/images/foto-reuniao-07-12-13.jpg =600x)
