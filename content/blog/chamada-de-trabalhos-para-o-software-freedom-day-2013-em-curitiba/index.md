---
kind: article
created_at: 2013-08-08
title: Chamada de trabalhos para o Software Freedom Day 2013 em Curitiba  
image_file: banner-sfd-2013.png
image_alt: SFD
author: Paulo Henrique de Lima Santana
short_description: |
 A Comunidade Curitiba Livre está aceitando propostas de palestras para o Software Freedom Day 2013, que acontecerá em Curitiba no dia 21 de setembro das 10:00h às 17:00 no campus da FESP - Faculdade de Educação Superior do Paraná. 
---

A Comunidade Curitiba Livre está aceitando propostas de palestras para o Software Freedom Day 2013, que acontecerá em Curitiba no dia 21 de setembro das 10:00h às 17:00 no campus da FESP - Faculdade de Educação Superior do Paraná.

O Software Freedom Day (SFD) ou em português, Dia da Liberdade do Software é uma celebração mundial do Software Livre e de Código Aberto
(SL/CA).O objetivo com essa festa é mostrar ao público em todo o mundo os benefícios de usar SL/CA de alta qualidade na educação, no governo, em casa, e nos negócios, ou seja, em todos os lugares!

Para ver mais informações e enviar a sua proposta até **25 de agosto**, acesse:

<http://softwarelivre.org/sfd2013-curitiba>
