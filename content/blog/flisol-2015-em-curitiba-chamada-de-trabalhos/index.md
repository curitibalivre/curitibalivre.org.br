---
kind: article
created_at: 2015-03-08
title: FLISOL 2015 em Curitiba - chamada de trabalhos 
image_file: banner-flisol-2015.png
image_alt: Flisol 2015
author: Adriana Cássia da Costa
short_description: |
  A 11a. edição do FLISOL - Festival Latino-americano de Instalação de Software Livre, acontecerá no dia 25 de abril de 2015 em várias cidades, incluindo Curitiba.
---

A 11a. edição do **FLISOL - Festival Latino-americano de Instalação de Software Livre**, acontecerá no dia 25 de abril de 2015 em várias cidades, incluindo Curitiba.

A Comunidade Curitiba Livre está iniciando a chamada de trabalhos para os interessados em palestrar.

O envio de proposta de palestra ou oficina deve ser feito até o dia 29 de março de 2015.

O FLISOL 2015 acontecerá no Campus da Pontifícia Universidade Católica do Paraná - [PUCPR](http://www.pucpr.br/) no Bloco Amarelo.

Endereço: Rua Imaculada Conceição, 1.155, Prado Velho - Curitiba PR

Para conhecer mais sobre o evento, enviar proposta de palestra/oficina, acesse:

<http://softwarelivre.org/flisol2015-curitiba/>
