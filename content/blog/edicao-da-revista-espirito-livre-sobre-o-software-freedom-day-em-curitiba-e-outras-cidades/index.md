---
kind: article
created_at: 2015-02-28
title: Edição da Revista Espírito Livre sobre o Software Freedom Day em Curitiba e outras Cidades 
image_file: banner-sfd-2015-revista.png
image_alt: SFD 2015
author: Adriana Cássia da Costa
short_description: |
  Foi lançada a Edição Especial da Revista Espírito Livre sobre o Software Freedom Day e a Comunidade Curitiba marcou presença.
---

Foi lançada a Edição Especial da **Revista Espírito Livre** sobre o **Software Freedom Day** e a Comunidade Curitiba marcou presença.

Confira o que aconteceu no SFD em Curitiba e em outros estados brasileiros.

Este ano o Software Freedom Day vai acontecer no dia 19 de setembro. Organize na sua cidade também! 

<http://softwarefreedomday.org>

Faça o Download da revista clicando [AQUI](http://www.revista.espiritolivre.org/lancada-edicao-n-67-da-revista-espirito-livre/).

![Banner](/images/sfd-revista-2015.jpg)
