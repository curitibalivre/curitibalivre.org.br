---
kind: article
created_at: 2017-06-06
title: Fotos da palestra do Stallman em Curitiba 
image_file: banner-stallman-2017.png
image_alt: Stallman em Curitiba
author: Paulo Henrique de Lima Santana
short_description: |
  As fotos da palestra do Richard Stallman realizada no dia 02 de junho de 2017 no auditório Prof. Ulysses de Campos do Setor de Ciências Sociais Aplicadas da Universidade Federal do Paraná estão aqui:
---

As fotos da palestra do Richard Stallman realizada no dia 02 de junho de 2017 no auditório Prof. Ulysses de Campos do Setor de Ciências Sociais Aplicadas da Universidade Federal do Paraná estão aqui:

<https://www.flickr.com/photos/curitibalivre/sets/72157684615095686>


![Stallman em Curitiba](/images/stallman-curitiba-2017.jpg =600x)

