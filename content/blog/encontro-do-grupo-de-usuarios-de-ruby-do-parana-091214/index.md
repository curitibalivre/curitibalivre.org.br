---
kind: article
created_at: 2014-11-25
title: Encontro do Grupo de Usuários de Ruby do Paraná 09/12/14 
image_file: banner-encontro-guru-pr.png
image_alt: GURU
author: Paulo Henrique de Lima Santana
short_description: |
  Divulgado na lista do GURU-PR (Grupo de Usuários de Ruby do Paraná):
---

Divulgado na [lista do GURU-PR](https://groups.google.com/forum/#!forum/guru_pr) (Grupo de Usuários de Ruby do Paraná):

Está marcado mais um encontro para o pessoal do grupo bater um papo.

Será no dia 09/12 a partir das 18h30 no Sal Grosso (Rua Doutor Claudino Santos, 59 - São Francisco, Curitiba - PR). O bar fica no Largo da Ordem.

Espero todos lá!

Tiago Lupepic
