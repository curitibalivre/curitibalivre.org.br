---
kind: article
created_at: 2013-11-15
title: Convite para o Fórum 23/11/2013   
image_file: banner-forum-permanente.png
image_alt: Forum
author: Paulo Henrique de Lima Santana
short_description: |
 Nos últimos tempos, pode-se constatar que o avanço da tecnologia da informação contribuiu para aumentar as opções de Softwares Livres e Proprietários disponíveis no mercado.
---

Nos últimos tempos, pode-se constatar que o avanço da tecnologia da informação contribuiu para aumentar as opções de Softwares Livres e Proprietários disponíveis no mercado. Os usuários tem a liberdade de escolher as opções adequadas para o seu uso profissional e pessoal, assim como os governos, porém, as deliberações decorrentes de políticas públicas  afetam diretamente todos os cidadãos.

A adequação das políticas públicas às necessidades coletivas é um ponto que deve ser tratado em todos os setores da sociedade. As decisões dos governantes devem estar em conformidade com os anseios dos cidadãos. Em se tratando do uso de softwares, esse é um fator para reflexão. O processo de seleção dos softwares para o uso público torna-se um assunto de importância ímpar em nosso cotidiano, bem como a manutenção de políticas que visem a difusão da tecnologia associada a redução de custos para permitir o acesso a tecnologia principalmente para os indivíduos em situação de fragilidade social.

A primeira edição do Fórum Permanente em Prol do Software Livre no Paraná tem por objetivo refletir sobre as políticas públicas e a adesão ao uso de Software Livres em nosso estado. O evento é organizado pela comunidade Curitiba Livre e ocorrerá no dia 23 de Novembro de 2013.

A sua presença irá enriquecer esse debate! Quem mora em outras cidades poderá acompanhar a transmissão online.

Na primeira edição estarão presentes os dos seguintes debatedores:

* Deputado Pastor Edson Praczyk (PRB) - autor da leis estaduais sobre Software Livre (Leis 14058 e 14195) e Open Document Format ODF (Lei 15742).
* Prof. Dr. Marcos Castilho - Chefe do Departamento de Informática da UFPR.
* Rodrigo Robles - membro da comunidade Curitiba Livre.
* Eng. Msc. Juliano Bueno de Araújo - Secretário Executivo do Fórum do Movimento Ambientalista do Paraná.

Detalhes:

* Horário: 14:00 horas
* Local: Auditório do Fórum Ambientalista do Paraná.
* Endereço: Rua Gaspar Carrilho Júnior, 001 - Jardim Schaffer
* Vista Alegre/Bom Retiro (anexo ao Bosque Gutierrez)

Mais informações e inscrição: <http://softwarelivre.org/forum-permanente-parana>
