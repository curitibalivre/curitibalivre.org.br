---
kind: article
created_at: 2013-11-05
title: Reunião na GaragemHacker 06/11/13   
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 Gostariamos de convidar a todos da comunidade hacker e de software livre de Curitiba para conversarmos sobre a GaragemHacker e projetos em andamento e futuros projeto.
---

![Garagem](/images/banner-gh.png =600x)
