---
kind: article
created_at: 2015-09-20
title: Fotos do Software Freedom Day 2015 em Curitiba SFD2015  
image_file: banner-foto-sfd2015.png
image_alt: Fotos SFD 2015
author: Paulo Henrique de Lima Santana
short_description: |
  Fotos do Software Freedom Day 2015 realizado ontem (19/09) na FATECPR.
---

Fotos do [Software Freedom Day 2015](http://softwarelivre.org/sfd2015-curitiba) realizado ontem (19/09) na [FATECPR](http://fatecpr.com.br):

<https://www.flickr.com/photos/curitibalivre/albums/72157656558658693>

Fotos que a Bárbara Tostes tirou:

<https://www.flickr.com/photos/10404262@N05/sets/72157658450356190>

E um texto que ela fez para o site SempreUpdate:

<http://sempreupdate.org/software-freedom-day-curitiba-tem-172-inscritos>

