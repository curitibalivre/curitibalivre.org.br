---
kind: article
created_at: 2014-08-21
title: Comunidade de Software Livre de Curitiba participará da 12ª edição da Feira de Cursos e Profissões da UFPR   
image_file: banner-feira-cursos-2014.png
image_alt: Feira
author: Paulo Henrique de Lima Santana
short_description: |
  De 22 a 24 de agosto de 2014 acontecerá a 12ª edição da Feira de Cursos e Profissões da Universidade Federal do Paraná no Setor de Educação Profissional e Tecnológica.
---

De **22 a 24 de agosto de 2014** acontecerá a [12ª edição da Feira de Cursos e Profissões](http://www.feiradecursos.ufpr.br/) da Universidade Federal do Paraná no Setor de Educação Profissional e Tecnológica.

**A Comunidade Curitiba Livre ocupará um stand para divulgar o Software Livre e a Liberdade do Conhecimento.** Como já havia acontecido em 2013, a organização da Feira gentilmente disponibilizou este espaço, e você pode ver as [fotos do ano passado](http://curitibalivre.org.br/feira-ufpr-2013/).

Qualquer pessoa pode ir lá ajudar a Comunidade conversando com os visitantes e distribuindo os panfletos sobre Software Livre que a UFPR imprimiu para nós. São esperados mais de 50 mil pessoas nos três dias de evento!

Para quem não conhece a iniciativa, todos os anos a UFPR organiza esta Feira para que a comunidade em geral (especialmente estudantes do ensino médio) possa conhecer os cursos que são ofertados no vestibular. Cada curso tem um stand e os alunos ficam lá mostrando as "coisas" que eles fazem e tirando dúvidas da garotada e dos pais. Durante os dias chegam ônibus de escolas inclusive do interior do Paraná e de outros estados, e claro, pais que levam os filhos para que eles possam pensar sobre o que fazer no futuro.

Ano passado tivemos vários estudantes dizendo que já tinham usado GNU/Linux nas escolas estaduais. Outros que aprenderam por conta própria e gostariam de se envolver na comunidade ou em projetos. Pais que usam e queriam que os filhos também usassem. Professores que usam ou usavam também nas escolas, etc. É realmente uma experiência muito enriquecedora.

Enfim, que tiver uma tempinho livre nesse final de semana, pode participar na sexta, sábado e/ou domingo. Mesmo que você tenha apenas uma tarde livre, já ajudará muito!

Endereço da Feira: Rua Alcides Vieira Arcoverde, 1225 - Jardim das Américas - Curitiba.

O stand de Software Livre é o de número 31. Veja o mapa da Feira abaixo:

![Panfleto](/images/mapa-feira-2014.png)
