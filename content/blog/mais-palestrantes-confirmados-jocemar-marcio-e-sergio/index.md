---
kind: article
created_at: 2014-06-27
title: Mais palestrantes confirmados Jocemar, Márcio e Sérgio     
image_file: banner-semana-2014.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 Veja abaixo mais três membros ativos da comunidade de Software Livre que confirmaram presença no evento.
---

Veja abaixo mais três membros ativos da comunidade de Software Livre que confirmaram presença no evento.

* **Jocemar do Nascimento** é Coordenador do Núcleo de Tecnologia Educacional Municipal de Cascavel, Graduado em Pedagogia pela CESUMAR. Pós Graduando em Docência no Ensino Médio, Técnico e Superior. Servidor da Prefeitura Municipal de Cascavel desde de 2008, sendo a 4 anos na Coordenação do NTM de Cascavel ministrando cursos na área de informática educacional com Software Livre. Voluntário do Projeto  CodeClubBrasil.org.

* **Márcio Junior Vieira** é fundador e CEO da Ambiente Livre Tecnologia. Desenvolvedor de Software, trabalha com Software Livre desde 2000. Formado em Tecnologia em Informática (UFPR), Pós-Graduado em Software Livre (UFPR). Palestrante em diversos em congressos relacionados a Software Livre: CONISLI, SOLISC, FISL, LATINOWARE, FLISOL, SFD, Pentaho Day e Joomla Day. Especialista implantação e customização de Pentaho, Hadoop, Alfresco, LimeSurvey, Joomla, SugarCRM e dotProject.

* **Sérgio Luís Bertoni** é Presidente da Fundação Blogoosfero e Coordenador de TIE-Brasil desde 1998. Bacharel em Filosofia e Mestre em Filosofia pela Universidade Estatal de Moscou M.V.Lomonossov. Nascido em uma família operária, aos 14 anos estudou no SENAI na qualidade de trabalhador contratado pela Ford, onde se tornaria ferramenteiro e sindicalista, militante da Oposição Sindical Metalúrgica de SP. Aos 21 anos foi eleito Secretário Geral da Comissão de Fábrica dos Trabalhadores na Ford, de onde, em 1988, sairia para estudar Filosofia Social na Universidade Estatal de Moscou, na URSS. Em terras soviéticas, e mais tarde russas, além de estudar dedicou-se também à formação de sindicalistas russos, criando TIE-Moscow (Troca de Informações sobre Empresas Transnacionais) que promove o intercâmbio de informações e experiências entre trabalhadores estrangeiros e locais, preparando-os para os embates (que viriam a ter neste século XXI) com as empresas transnacionais que se instalaram na Rússia após o fim da União Soviética. Mestre em Filosofia Social e de volta ao Brasil em 2001, deu continuidade ao trabalho de troca de informações e experiências entre trabalhadores em empresas transnacionais. Em 2005 criou o site de TIE-Brasil, que seria listado por Emir Sader, anos mais tarde, como uma das 10 fontes de informação alternativa na internet brasileira. Na condição de editor e administrador do site de TIE-Brasil, em 2010 participou do 1º Encontro Nacional de Blogueiros. No ano seguinte faria parte da comissão organizadora do 1º Encontro Estadual de Blogueiros Progressistas no Paraná e desde então um dos coordenadores do projeto Blogoosfero.
