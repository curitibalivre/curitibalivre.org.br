---
kind: article
created_at: 2014-09-25
title: Fotos da 1a Semana de Software Livre de Curitiba     
image_file: banner-semana-2014.png
image_alt: Cartilha
author: Paulo Henrique de Lima Santana
short_description: |
  Veja as fotos da 1a Semana de Software Livre de Curitiba.
---

Veja as fotos da 1a Semana de Software Livre de Curitiba, evento que reuniu o [VI Fórum de Tecnologia em Software Livre](http://www.ftsl.org.br/) nos dias 18 e 19 de setembro e o [Software Freedom Day](http://softwarelivre.org/sfd2014-curitiba) no dia 20 de setembro, no campus central da [Universidade Tecnológia Federal do Paraná](http://www.utfpr.edu.br/).

A Comunidade Curitiba Livre participou da organização em parceria com os funcionários do Serpro e da UTFPR.

<https://www.flickr.com/photos/curitibalivre>

![Debate](/images/foto-semana-2014.jpg =600x)
