---
kind: article
created_at: 2015-07-27
title: 3ª etapa do Circuito Curitibano de Software Livre - 05 de agosto na Uniandrade
image_file: banner-circuito-3-uniandrade.png
image_alt: Circuito 3 Uniandrade
author: Paulo Henrique de Lima Santana
short_description: |
  A 3ª etapa do Circuito Curitibano de Software Livre acontecerá na Uniandrade, no dia 05 de agosto de 2015 das 19:00h às 22:00h.
---

A **3ª etapa do Circuito Curitibano de Software Livrei** acontecerá na Uniandrade, no dia 05 de agosto de 2015 das 19:00h às 22:00h.

Serão 4 palestras apresentadas por membros da comunidade de Software Livre de Curitiba sobre os conceitos de [Software Livre](http://www.gnu.org/) e a Comunidade Curitiba Livre, a distribuição [Debian GNU/Linux](http://www.debian.org/), a suíte para escritórios [LibreOffice](https://pt-br.libreoffice.org/), e negócios em FLOSS (Free/Libre/Open Source Software).

O Circuito Curitibano de Software Livre é totalmente gratuito e nesta 3ª etapa é exclusivo para estudantes, professores e funcionários da Uniandrade.

Aqueles que quiserem participar do sorteio dos brindes, deverão preencher o formulário de inscrição e estar presentes no local no momento do encerramento do evento. Nesta etapa, contamos com o apoio do Nic.br que doou um roteador wi-fi do projeto SIMETBox para sorteio. Também teremos algumas camisetas e outros brindes..

Para saber mais sobre o projeto Circuito Curitibano de Software Livre leia a nossa apresentação e se tiver interesse em levar o evento para a sua Faculdade/Universidade, entre em contato conosco através do email: <contato@curitibalivre.org.br>

![Circuito 3 Uniandrade](/images/logos-circuito-3.png)

