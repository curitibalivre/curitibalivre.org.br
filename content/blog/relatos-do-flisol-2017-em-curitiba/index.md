---
kind: article
created_at: 2017-04-16
title: Relatos do FLISOL 2017 em Curitiba
image_file: banner-flisol-2017.png
image_alt: Flisol 2017
author: Paulo Henrique de Lima Santana
short_description: |
  O FLISOL 2017 em Curitiba foi realizado no dia 08 de abril, das 9:00h às 17:00h no prédio do Setor de Educação Profissional e Tecnológica da Universidade Federal do Paraná (SEPT-UFPR).
---

O [FLISOL 2017 em Curitiba](http://softwarelivre.org/flisol2017-curitiba) foi realizado no dia 08 de abril, das 9:00h às 17:00h no prédio do Setor de Educação Profissional e Tecnológica da Universidade Federal do Paraná (SEPT-UFPR).

Na primeira reunião da organização que aconteceu ainda no ano passado, decidimos diminuir o tamanho do evento que em [2014](http://softwarelivre.org/flisol2014-curitiba), [2015](http://softwarelivre.org/flisol2015-curitiba) e [2016](http://softwarelivre.org/flisol2016-curitiba) foi realizado na PUC-PR. Apesar de nestes três anos termos conseguido um grande número de participantes, todo o trabalho necessário para isso gerou muito desgaste no grupo a ponto de nos perguntarmos se tudo aquilo realmente valia a pena. Por isso, a decisão do grupo para 2017 foi, por exemplo, usar apenas um auditório para as palestras e duas salas para as oficinas. Também decidimos não emitir certificados de participação, o que provavelmente implicaria na diminuição do número de participantes, especialmente de estudantes que vão apenas por causa disso. Então o *downsizing* do FLISOL 2017 em Curitiba foi um objetivo planejado.

Além disso, como já é tradição desde que o FLISOL foi realizado a primeira vez em 2005, mudamos o local do evento para promover o rodízio pela cidade. O FLISOL já aconteceu no Colégio Estadual do Paraná (2005, 2006 e 2007), nas Faculdades Santa Cruz (2008, 2009 e 2010), na FESP (2012 e 2013), na PUC-PR (2014, 2015 e 2016) e este ano no SEPT-UFPR.

### Números finais

Os números do FLISOL 2017 em Curitiba são os seguintes:

* Palestras: 07
* Oficinas: 03
* Instalações: 06
* Inscritos: 134
* Participantes presentes: 69


### Fotos

Veja no link abaixo as fotos do FLISOL 2017 em Curitiba:

* <https://www.flickr.com/photos/curitibalivre/albums/72157679059866304>

### Agradecimentos

Agradecemos ao Prof. Razer Montaño por nos ajudar a sediar o FLISOL 2017 no [SEPT - UFPR](http://200.17.200.17/).

Agradecemos a [Associação dos Professores da UFPR (APUFPR)](http://apufpr.org.br/) por patrocinar o FLISOL 2017 em Curitiba.

Agradecemos aos palestrantes e instrutores abaixo que contribuiram com seus conteúdos para as atividades:

* Adriana Cássia da Costa
* Antonio Terceiro
* Cleber Ianes
* Fredy Schaible
* Gustavo De Nardin
* Leandro Henrique dos Reis
* Leonardo Rodrigues
* Patty Vader
* Rafaela Raganham
* Ricardo Fantin da Costa

Por fim, agradecemos a todos os voluntários abaixo que ajudaram na realização do FLISOL 2017 em Curitiba:

* Adriana Cássia da Costa
* Angelo Rosa
* Bárbara Tostes
* Cleber Ianes
* Daniel Lenharo
* Fabianne Balvedi
* Gilmar Nascimento
* Leonardo Rodrigues
* Paulo Henrique de Lima Santana
* Rodrigo Robles
* Samuel Henrique


***A organização do FLISOL 2017 em Curitiba agradece a presença de todos e considera esta edição novamente um sucesso.***

***Nos vemos em 2018!***

![Foto participantes](/images/foto-flisol-2017.jpg =600x)

