---
kind: article
created_at: 2015-07-21
title: Comunidades GruPy-PR e GURU-PR fazem encontros em Curitiba
image_file: banner-grupy-guru-pr.png
image_alt: GruPy-PR e Guru-PR
author: Paulo Henrique de Lima Santana
short_description: |
  No final de julho e começo de agosto, dois grupos de software livre de Curitiba realizarão encontros na cidade.
---

### GruPy-PR

O [Grupo de usuário Python do Paraná (GruPyPR)](https://groups.google.com/forum/#!forum/grupy-pr)  fará um encontro no dia 29 de julho a partir das 19:00h na sede da [Garagem Hacker](http://garagemhacker.org/blog).

Segundo o que está publicado na [página do evento no Meetup](http://www.meetup.com/GruPy-PR/events/224076877): "o próximo encontro  daremos continuidade a aplicação de bike messenger e sobre futuros projetos do grupo."

* <https://github.com/GruPyPR/cwbmess>
* <https://github.com/GruPyPR/encontros>

A Garagem Hacker fica localizada na Rua Antônio Chella, 431 - Bom Retiro.

### GURU-PR

O [Grupo de Usuários de Ruby do Paraná (GURU-PR)](http://www.gurupr.org) está organizando o [1º Tech Day do GURU-PR](http://www.gurupr.org/eventos/1-tech-day-do-guru-pr) no dia 08 de agosto a partir das 8h30min na EBANX.

A inscrição gratuita pode ser feita no site do evento: <https://eventioz.com.br/e/1-tech-day-guru-pr>.

A EBANX fica localizada Rua Mal. Deodoro, 630 - Centro.
