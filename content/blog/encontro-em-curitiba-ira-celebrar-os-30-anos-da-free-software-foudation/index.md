---
kind: article
created_at: 2015-09-25
title: Encontro em Curitiba irá celebrar os 30 anos da Free Software Foudation  
image_file: banner-fsf30-curitiba.png
image_alt: FSF 30 anos Curitiba
author: Paulo Henrique de Lima Santana
short_description: |
  A Comunidade Curitiba Livre convida a todos para participarem da comemoração dos 30 anos da Free Software Foudation. Para celebrar essa data, estamos organizando um encontro no dia 03 de outubro de 2015 (sábado) das 13:00h às 18:00h na Aldeia Coworking.
---

A **Comunidade Curitiba Livre** convida a todos para participarem da comemoração dos 30 anos da [Free Software Foudation](http://www.fsf.org).

Para celebrar essa data, estamos organizando um encontro no dia 03 de outubro de 2015 (sábado) das 13:00h às 18:00h na [Aldeia Coworking](https://aldeia.cc).

### Programação

Durante toda a tarde iremos promover atividades que buscam a integração das comunidades de Curitiba e incentivar o uso e desenvolvimento de software livre, e a liberdade do conhecimento. A programação terá:

* **Apresentações das comunidades de Curitiba:** grupos ativos de Curitiba apresentarão os trabalhos que tem realizado e o que pretendem fazer no futuro. Os grupos são: PyLadies, [GruPy-PR (Grupo de Usuário de Python do Paraná)](https://github.com/GruPyPR), [Garagem Hacker](http://garagemhacker.org), [GURU-PR (Grupo de Usuários de Ruby do Paraná)](http://www.gurupr.org), [WordPress Curitiba](http://www.meetup.com/wpcuritiba/?_locale=pt), [PHP Parana](http://www.phppr.net), e [Comunidade Curitiba Livre](http://curitibalivre.org.br).
* **Palestras relâmpago:** apresentações de no máximo 5 minutos sobre temas diversos.
* **Roda de conversa sobre Software Livre:** discutir os conceitos de Software Livre, e os participantes poderão comentar sobre como usam software livre, onde poderiam usar, etc.
* **Como contribuir com projetos de Software Livre:** discutir formas de como contribuir com projetos de Software Livre.
* **Discussao sobre próximos eventos de Software Livre em Curitiba:** Discutir a realização dos eventos que poderao acontecer em 2016 como o Hardware Freedom Day (HFD), Document Freedom Day (DFD), Education Freedom Day (EFD), FLISOL - Festival Latino-americano de Intalaçao de Software Livre, Debian Day, Software Freedom day (SFD), etc. Questionar o pessoal sobre qual tema eles gostariam que fosse abordado nos próximos eventos, assim poderíamos organizar grupos de estudo, ou encontrar alguém que domine o assunto e possa ministrar uma palestra/oficina futuramente.

No final de encontro pretendemos fazer uma pequena festa com bolo e refrigerante :-)

### Inscrição

Pedimos que por gentileza preencha o formulário para que possamos saber quantas pessoas estarão presentes no encontro e assim possamos planejar melhor a nossa festa :-)

Lembramos que não será emitido certificado de participação.

### Resumindo

**Encontro em Curitiba para celebrar os 30 anos da Free Software Foudation.**

* Data: 03 de outubro de 2015
* Horário: 13:00h às 18:00h
* Local: Aldeia Coworking
* Endereço: Rua Marechal Deodoro, 262 - 1º andar - Galeria Suissa - Centro.
* Mais informações: <http://fsf30.curitibalivre.org.br>

