---
kind: article
created_at: 2014-11-17
title: Encontro Rede Livre 13/12/2014  
image_file: banner-encontro-rede-livre.png
image_alt: Encontro Rede Livre
author: Paulo Henrique de Lima Santana
short_description: |
  Para fechar ano, a Rede Livre está organizando um encontro colaborativo entre hackers, movimentos e organizações sociais e culturais no dia 13 de dezembro de 2014 (sábado) a partir das 10:00h na sede da Ethymos.
---

Para fechar ano, a [Rede Livre](http://redelivre.org.br/) está organizando um encontro colaborativo entre hackers, movimentos e organizações sociais e culturais no dia 13 de dezembro de 2014 (sábado) a partir das 10:00h na sede da [Ethymos](http://redelivre.org.br/).

Você pode sugerir atividades no nosso pad: <http://piratepad.net/EncontroRedeLivre20141213>

## Atividades durante todo o dia:

* Install Fest GNU/Linux - Traga seu computador que te libertamos do Windows
* Churras carnívorx ou vegetarianx! Traga sua comida/bebida!

## Roda de conversas:

Temas a definir no [pad](http://piratepad.net/EncontroRedeLivre20141213)

### Frentes / Trilhas:

* Agroecologia
* Comunicação
* Cultura
* Hacker / Software Livre

## Sarau Cultural

Para quem quiser promover alguma atividade cultural como cantar, tocar instrumento, recitar um poema, etc.

## Divulgação dos eventos de Software Livre em 2015:

* O futuro da web/espionagem/redes livres - data a definir
* Hardware Freedom Day - 17 de janeiro
* Education Freedom Day - 21 de março
* Document Freedom Day - 25 de março
* FLISOL -  25 de abril
* Culture Freedom Day - 16 de maio
* Debian Day - 16 (ou 15) de agosto
* Software Freedom Day - 19 de setembro

## Resumindo:

* Data: 13 de dezembro de 2014 (sábado)
* Horário: a partir das 10:00h
* Local: Ethymos Comunicação
* Endereço: Rua Itupava, 1299 – sala 312
* Dúvidas e informações: phls@ethymos.com.br

