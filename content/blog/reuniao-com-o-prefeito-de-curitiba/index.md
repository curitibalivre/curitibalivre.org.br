---
kind: article
created_at: 2013-10-14
title: Reunião com o Prefeito de Curitiba   
image_file: banner-reuniao-prefeitura.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 A convite do Deputado Estadual Pastor Edson Praczyk participamos de uma audiência hoje (14/10/13) com o Prefeito de Curitiba Gustavo Fruet. 
---

A convite do Deputado Estadual Pastor Edson Praczyk participamos de uma audiência hoje (14/10/13) com o Prefeito de Curitiba Gustavo Fruet. Esteve representando a comunidade Curitiba Livre na reunião o Paulo Henrique de Lima Santana.

A conversa foi bem rápida, algo em torno de meia-hora mas deu para
falar um pouco sobre a comunidade e os eventos que organizamos.

O Prefeito comentou sobre as questões do ICI e da URBS (código fonte do
sistema de bilhetagem) e sobre a importância de ter pequenas e médias
empresas desenvolvendo software livre aqui na região.

Comentamos que tínhamos visto a notícia do lançamento do Conexão
Educacional e que já tínhamos participado de uma reunião na SME. Ele
disse que além da SME, a Secretaria de Saúde tem investido também.

Enfim, foi mais uma apresentação para dizer que a comunidade de
software livre está a disposição para colaborar com a Prefeitura de Curitiba.

Ficamos de convidá-lo para o [Fórum Permanente em Prol do Software Livre no Paraná](http://softwarelivre.org/forum-permanente-parana) e ele ficou de agendar uma reunião nossa com o Secretário de Tecnologia de Curitiba Paulo Miranda pra ver como podemos ajudar.
