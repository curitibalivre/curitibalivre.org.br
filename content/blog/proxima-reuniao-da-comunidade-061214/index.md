---
kind: article
created_at: 2014-12-02
title: Próxima reunião da Comunidade 06/12/14
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
  Iremos realizar a próxima reunião da Comunidade novamente na sede da Ethymos.
---

Iremos realizar a próxima reunião da Comunidade novamente na sede da Ethymos.

Resumindo:

* Reunião da Comunidade Curitiba Livre
* Data: 06/12 (sábado)
* Horário: 10:00h
* Local: Ethymos
* Endereço: Rua Itupava, 1.299 - sala 312 - Curitiba

Sugestão de pauta:

* Planejamento para 2015

Obs: A reunião é aberta a todos os interessados. É só vir e participar :-)

[Apresentação usada na reunião sobre as atividades em 2014 (odp)](/arquivos/reuniao-curitibalivre-06-12-2014.odp)

[Apresentação usada na reunião sobre as atividades em 2014 (pdf)](/arquivos/reuniao-curitibalivre-06-12-2014.pdf)
