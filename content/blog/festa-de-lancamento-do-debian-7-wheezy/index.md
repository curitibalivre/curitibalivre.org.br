---
kind: article
created_at: 2013-05-07
title: Festa de Lançamento do Debian 7 Wheezy  
image_file: banner-festa-lancamento-debian-7.png
image_alt: Debian
author: Paulo Henrique de Lima Santana
short_description: |
 Para celebrar o lançamento da versão 7.0 do Debian GNU/Linux nova versão (codinome "Wheezey") a comunidade de Software Livre de Curitiba irá se reunir neste sábado (07 de maio de 2013) no auditório da Associação dos Professores da UFPR. 
---

Para celebrar o lançamento da versão 7.0 do Debian GNU/Linux nova versão (codinome "Wheezey") a comunidade de Software Livre de Curitiba irá se reunir neste sábado (07 de maio de 2013) no auditório da Associação dos Professores da UFPR.

Você pode trazer seu computador/notebook para instalar a nova versão. Ou venha apenas encontrar a galera e bater um papo sobre software livre.

* **Quando**: Sábado, 11 de Maio de 2013 - 14:00h
* **Onde**: Auditório APUFPR - Associação dos Professores da UFPR - Rua Dr. Alcides Vieira Arcoverde, 1.305 - Jardim das Américas - Curitiba - PR (ao lado da Escola Técnida da UFPR). Mapa.

Se você prendente participar, coloque seu nome na lista:

<http://piratepad.net/LancamentoWheezyCuritiba>

Nota oficial sobre o lançamento do Wheezy:

<http://www.debian.org/News/2013/20130504>

Lista de discussão da comunidade de software livre de Curitiba:

<curitibalivre@listas.softwarelivre.org>

![Minions](/images/lancamento-wheezy-curitiba.gif =600x)
 
Fotos do encontro:

<https://www.flickr.com/photos/curitibalivre/albums/72157655012357874>
