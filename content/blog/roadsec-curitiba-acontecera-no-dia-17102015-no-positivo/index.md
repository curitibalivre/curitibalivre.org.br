---
kind: article
created_at: 2015-10-09
title: Roadsec Curitiba acontecerá no dia 17/10/2015 no Positivo 
image_file: banner-roadsec-2015.png
image_alt: Roadsec 2015
author: Paulo Henrique de Lima Santana
short_description: |
  O maior evento brasileiro de Hacking, Segurança e Tecnologia volta à Curitiba, na última edição do ano antes do grande encerramento!
---

O maior evento brasileiro de Hacking, Segurança e Tecnologia volta à Curitiba, na última edição do ano antes do grande encerramento! Pra fechar com chave de ouro, novidades te esperam no [Roadsec](http://roadsec.com.br/curitiba2015/) em 2015, e você tem um convite de honra para conferir o que há de melhor. Venha aproveitar o sábado épico do dia 17 de outubro conosco, não perca!

ATENÇÃO: O Roadsec concede certificado de Horas Complementares.

* PALESTRAS: O evento está recheado de palestrantes de altíssimo nível, que vieram de todo o país! Aprenda, debata, faça novos contatos e fortaleça seu networking como nunca. É hora de valorizar sua carreira!
* OFICINAS: Disputadas oficinas estarão no laboratório do Roadhack: Robótica Mindstorms, controle de drones, corrida em autorama de inteligência artificial, técnicas de lock picking, construção de circuitos programáveis e mais!
* DESAFIOS: O maior campeonato de Hacking do Brasil está no Roadsec! O HACKAFLAG (campeonato Capture the Flag) te desafia a mostrar o seu talento. Os vencedores de cada estado ganham viagem com tudo pago para a grande final em São Paulo!

E tem mais: A Cryptorace voltou com tudo esse ano. Recheada de prêmios incríveis a cada edição, a corrida criptográfica do Roadsec também leva o grande campeão para a mega edição de São Paulo com tudo pago!

Não fique de fora! As vagas são limitadas.
 
**CÓDIGO EXCLUSIVO DE DESCONTO: RSPR-JHA8391K7**

[INSCREVA-SE AQUI!!!](http://roadsec.com.br/curitiba2015)

![Banner Roadsec](/images/roadsec-convite-2015.jpg =600x)
