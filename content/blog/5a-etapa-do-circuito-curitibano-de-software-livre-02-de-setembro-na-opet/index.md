---
kind: article
created_at: 2015-08-28
title: 5ª etapa do Circuito Curitibano de Software Livre - 02 de setembro na Opet
image_file: banner-circuito-5-opet.png
image_alt: Circuito 5 Opet
author: Paulo Henrique de Lima Santana
short_description: |
  A 5ª etapa do Circuito Curitibano de Software Livre acontecerá na Opet, no dia 02 de setembro de 2015 das 19h00min às 22h30min.
---

A **5ª etapa do Circuito Curitibano de Software Livre** acontecerá na [Opet](http://www.opet.com.br), no dia 02 de setembro de 2015 das 19h00min às 22h30min.

Serão 5 palestras apresentadas por membros da comunidade de Software Livre de Curitiba sobre os conceitos de [Software Livre](http://www.gnu.org/) e a Comunidade Curitiba Livre, a distribuição [Debian GNU/Linux](http://www.debian.org/), a linguagem de programação Ruby, o reforço do conceito de que Software Livre não é sinônimo de software gratuito, e negócios em FLOSS (Free/Libre/Open Source Software).

O Circuito Curitibano de Software Livre é totalmente gratuito e nesta 5ª etapa é exclusivo para estudantes, professores e funcionários da Opet.

Aqueles que quiserem participar do sorteio dos brindes, deverão estar presentes no local no momento do encerramento do evento. Nesta etapa, contamos com o apoio do Nic.br que doou um roteador wi-fi do projeto SIMETBox para sorteio. Também teremos algumas camisetas e outros brindes.

Veja a [programação completa](http://softwarelivre.org/circuito-curitibano/programacao-5a-etapa).

Para saber mais sobre o projeto Circuito Curitibano de Software Livre leia a nossa apresentação e se tiver interesse em levar o evento para a sua Faculdade/Universidade, entre em contato conosco através do email: <contato@curitibalivre.org.br>

![Circuito 5 Opet](/images/logos-circuito-5.png)

