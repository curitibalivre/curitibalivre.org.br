---
kind: article
created_at: 2015-03-12
title: Código de desconto para 17º Encontro Locaweb de Profissionais de Internet em Curitiba 17elw 
image_file: banner-17-locaweb.png
image_alt: Locaweb 17
author: Paulo Henrique de Lima Santana
short_description: |
  A Comunidade Curitiba Livre ganhou um código que dá 50% de desconto na inscrição para a 17ª edição do Encontro Locaweb de Profissionais de Internet que vai acontecer dia 11 de abril em Curitiba.
---

A Comunidade Curitiba Livre ganhou um código que dá **50% de desconto** na inscrição para a [17ª edição do Encontro Locaweb de Profissionais de Internet](http://eventos.locaweb.com.br/17o-encontro-locaweb-de-profissionais-de-internet-de-curitiba/) que vai acontecer dia 11 de abril em Curitiba.

Quem quiser o código, é só enviar um email para: contato@curitibalivre.org.br

Entre os confirmados está o André Noel do site [Vida de Programador](http://vidadeprogramador.com.br/). Vale a pena participar!
