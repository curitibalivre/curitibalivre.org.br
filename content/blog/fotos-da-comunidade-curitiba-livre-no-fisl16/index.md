---
kind: article
created_at: 2015-07-18
title: Fotos da Comunidade Curitiba Livre no FISL16 
image_file: banner-curitibalivre-fisl16.png
image_alt: Curitiba Livre FISL16
author: Paulo Henrique de Lima Santana
short_description: |
  Tiramos fotos da nossa turma na bancada da Comunidae Curitiba Livre na área de comunidades do FISL16.
---

Tiramos fotos da nossa turma na bancada da Comunidae Curitiba Livre na área de comunidades do [FISL16](http://softwarelivre.org/fisl16).

Estavam lá:

* Adriana da Costa
* Claudia Archer
* Cleber Ianes
* Daniel Lenharo
* Fabianne Balvedi
* Gisele Chemberge
* Márcio Júnior
* Paulo Henrique Santana
* Vitório Furusho

![Foto 1](/images/curitibalivre-fisl16-01.jpg =600x)

![Foto 3](/images/curitibalivre-fisl16-03.jpg =600x)

![Foto 4](/images/curitibalivre-fisl16-04.jpg =600x)

![Foto 5](/images/curitibalivre-fisl16-05.jpg =600x)

![Foto 8](/images/curitibalivre-fisl16-08.jpg =600x)

![Foto 2](/images/curitibalivre-fisl16-02.jpg =600x)

![Foto 6](/images/curitibalivre-fisl16-06.jpg)

