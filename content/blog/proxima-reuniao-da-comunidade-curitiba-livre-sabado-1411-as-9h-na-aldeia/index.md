---
kind: article
created_at: 2015-11-10
title: Próxima reunião da Comunidade Curitiba Livre sábado 14/11 às 9h na Aldeia
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião Curitiba Livre
author: Paulo Henrique de Lima Santana
short_description: |
  No próximo sábado (14/11) vamos nos reunir a partir das 9:00h na Aldeia Coworking para começar a planejar nossas ações em 2016.
---

No próximo sábado (14/11) vamos nos reunir a partir das 9:00h na [Aldeia Coworking](http://aldeiaco.com.br) para começar a planejar nossas ações em 2016.

Lembrando que a Aldeia mudou de sede e agora está na Av. Cândido de Abreu, 381.

Entre essas ações está principalmente a organização do [FLISOL](http://flisol.info) em abril. Existem outros eventos que podemos organizar como o [Document Freedom Day](http://documentfreedom.org) (março), [Education Freedom Day](http://www.educationfreedomday.org) (provavelmente em março), [Hardware Freedom Day](http://www.hfday.org) (sem data ainda), [Debian Day](https://wiki.debian.org/DebianDay/2016) (agosto), [Software Freedom Day](http://softwarefreedomday.org) (setembro). Também estamos articulando com a comunidade Debian para realizar uma [Mini-DebConf](https://wiki.debian.org/DebianEvents/br) aqui em Curitiba entre março e abril.

Queremos também manter uma relação cada vez mais próxima com as [comunidades ativas](http://curitibalivre.org.br/comunidades) em Curitiba como WordPress, Python e PyLadies (GruPy-PR), Blender, PHP, Ruby (GURU-PR), Garagem Hacker, quem sabe realizando um evento ainda no primeiro semestre de 2016 onde todos nós possam contribuir com a sua especialidade dentro da programação.

Mas tudo isso depende da ajuda de voluntários da comunidade, ou seja, de todos nós :-)

Enfim, todos estão convidados. Mesmo aqueles que nunca participaram das nossas reuniões ou atividades, podem ir lá para conhecer o pessoal.
