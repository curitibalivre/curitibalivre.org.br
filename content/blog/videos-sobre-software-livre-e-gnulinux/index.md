---
kind: article
created_at: 2015-03-18
title: Vídeos sobre Software Livre e GNU/Linux 
image_file: banner-videos-sl.png
image_alt: Vídeos sobre Software Livre e GNU/Linux
author: Paulo Henrique de Lima Santana
short_description: |
  Abaixo está uma coletânea de vídeos que contam um pouco da história do Software Livre e do GNU/Linux. São palestras, filmes, documentários, entrevistas e reportagens.
---

Abaixo está uma coletânea de vídeos que contam um pouco da história do Software Livre e do GNU/Linux. São palestras, filmes, documentários, entrevistas e reportagens.

Palestras do Richard Stallman no Brasil em 2017. Algumas tem áudio em português (tradução simultânea).

 * [Belo Horizonte - UFMG](http://stallman-brasil.softwarelivre.org/2017/belo-horizonte-ufmg)
 * [Brasília - Campus Party](http://stallman-brasil.softwarelivre.org/2017/brasilia-campus-party)
 * [Campinas - Unicamp](http://stallman-brasil.softwarelivre.org/2017/campinas-unicamp)
 * [Curitiba - UFPR](http://stallman-brasil.softwarelivre.org/2017/curitiba-ufpr)

Palestras do Richard Stallman no Brasil de 2000 a 2013

 * [2000](http://stallman-brasil.softwarelivre.org/2000)
 * [2012](http://stallman-brasil.softwarelivre.org/2012)
 * [2013](http://stallman-brasil.softwarelivre.org/2013)

Palestras do Jon Maddog em várias edições da Campus Party no Brasil (em inglês)

 * <http://hemingway.softwarelivre.org/videos-diversos/maddog-na-campus-party-brasil>

Entrevista: Richard Stallman a Profa. Beatriz Busaniche (sem legenda)

 * <https://www.youtube.com/watch?v=Hk4EiDbwGh8>

Entrevista: Richard Stallman Explains Everything (sem legenda)

 * <https://www.youtube.com/watch?v=jUibaPTXSHk>

Mensagem do Richard Stallman para o SFD 2016

 * <http://www.fsf.org/blogs/community/happy-software-freedom-day-a-few-words-from-rms>
 * <https://media.libreplanet.org/u/libreplanet/m/richard-stallman-explains-free-software-on-software-freedom-day-2016>
 * Legenda (Thadeu Cascardo e Adriano Rafael Gomes): [2016-SFD-V1-metadata2.srt](http://curitibalivre.org.br/arquivos/2016-sfd-v1-metadata2.srt)

Vários vídeos do Richard Stallman no site gnu.org:

 * <http://audio-video.gnu.org/video>

Richard Stallman na UFPR em Curitiba em 2012

 * <http://audio-video.gnu.org/video/palestra-stallman-ufpr.ogg>

Richard Stallman dançando com o GNU no FISL10

 * <https://www.youtube.com/watch?v=7t96m2ynKw0>

Entrevista do Richard Stallman em abril de 2016 (em inglês Singularity Blog)<br />

 * <https://www.singularityweblog.com/richard-stallman-singularity-free-software>
 * <https://goblinrefuge.com/mediagoblin/u/kyamashita/m/rms-interview-freedom-is-worth-the-inconvenience>

Entrevista do Richard Stallman em 2012 na UnB

 * <https://www.youtube.com/watch?v=FfrDTPIP-yY>

Richard Stallman - Software Livre: aspectos éticos , sociais e práticos (em espanhol) 2009

 * <https://dausacker.wordpress.com/2016/08/31/software-livre-aspectos-eticos-sociais-e-praticos-em-espanhol-richard-matthew-stallman/>

Documentário: InProprietário - O Mundo do Software Livre

 * <https://www.youtube.com/watch?v=7Yy0tFOKfQg>

Documentário: Revolution OS (legendado)

 * <https://www.youtube.com/watch?v=plMxWpXhqig>

Filme (animação de massinhas): O que é software Livre

 * <https://www.youtube.com/watch?v=UvWRhnc_77Y>

Animação sobre o que é Software Livre

 * <https://www.youtube.com/watch?v=jWq_x8noBYk>
 * Dublado: <https://www.youtube.com/watch?v=5hTj-fQzVus>

Richard Stallman no TEDx Geneva 2014 (legendado)

 * <http://softwarelivre.org/portal/fisl/video-do-richard-stallman-falando-sobre-software-livre-no-tedx-geneva-2014>
 * <https://www.youtube.com/watch?v=deiZ7v6g0hg>

Richard Stallman - mensagem para o SFD 2010

 * <https://www.youtube.com/watch?v=6-cZ1ED1Vu8>

Richard Stallman - entrevista em 2013 (legenda)

 * <https://www.youtube.com/playlist?list=PLEGeh9YTe7Fmr__0cNucmqwBTfnZ92M-n>

Richard Stallman - entrevista em 2012 ao canal RT (sem legenda)

 * <https://b2aeaa58a57a200320db-8b65b95250e902c437b256b5abf3eac7.ssl.cf5.rackcdn.com/media_entries/7633/Richard_Stallman_-_Were_heading_for_a_total_disaster-uFMMXRoSxnA.webm>

Stephen Fry - mensagem de feliz aniversário pelos 25 anos do GNU

 * <https://www.youtube.com/watch?v=lX2n6iYbqIM>

Jon Maddog Hall e o GNU/Linux Debian na Caixa Econômica Federal

 * <https://www.youtube.com/watch?v=xgreMMfrXf0>
 * Original: <http://antigo.estudiolivre.org/el-gallery_view.php?arquivoId=6626>

Filme (animação): Libertação do Usuário - 30 anos da Free Software Foundation (legendado)

 * <http://softwarelivre.org/portal/noticias/assista-o-video-comemorativo-aos-30-anos-da-fsf-libertacao-do-usuario>
 * <https://www.youtube.com/watch?v=brFdViPSsi4>

Vídeos diversos, incluindo uma mensagem do Richard Stallman

 * <http://wiki.softwarefreedomday.org/Resources#Websites_to_look_for_movies>

Breve História do Linux - Conectiva

 * <https://www.youtube.com/watch?v=qp87ZSNsKbw>

Maddog como corsário (legendado)

 * <https://www.youtube.com/watch?v=ViGntIpdpyw>

Documentário: Software Livre - ITI

 * <https://www.youtube.com/watch?v=IJrfcQq_eIw>

Filme (desenho): Copying Is Not Theft

 * Original: <https://www.youtube.com/watch?v=IeTybKL1pM4>
 * Legendado: <https://www.youtube.com/watch?v=R1QnXJYT9ao>
 * Duplado: <https://www.youtube.com/watch?v=LXilEPmkoQY>
 * Outros: <http://questioncopyright.org/minute_memes>

Reportagem do Olhar Digital: A história do Linux

 * <https://www.youtube.com/watch?v=ItzO5lUpDQE>

Reportagem: Governo boliviano promove uso de software livre

 * <http://youtu.be/LXOK4ldmDGI>

Reportagem: Dicas para ganhar dinheiro com software livre - FISL

 * <https://www.youtube.com/watch?v=1HjGSrFzdJY>

Reportagem: Uso de software livre gera economia de R$ 30 milhões ao governo federal

 * <https://www.youtube.com/watch?v=saEjztTXg7M>

Reportagem: Linus Torvalds na TV Globo

 * <https://www.youtube.com/watch?v=owrai0okB3A>

Reportagem: Novo software livre Educatux pode auxiliar o sistema educacional no país

 * <https://www.youtube.com/watch?v=1aBz4AI0yAQ>

Vídeos do Blender

 * Cycles Demoreel 2014: <https://www.youtube.com/watch?v=EJed22ShxLc>
 * The Best Blender Demo Reel Film 2015: <https://www.youtube.com/watch?v=yJe0iN5nu8g>


## Vídeos de palestras diversas

Você pode ver diversos vídeos de palestras em vários eventos em:

<http://videos.softwarelivre.org>
