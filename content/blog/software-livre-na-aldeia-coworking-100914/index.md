---
kind: article
created_at: 2014-09-03
title: Software Livre na Aldeia Coworking 10/09/14   
image_file: banner-palestra-aldeia.png
image_alt: Aldeia
author: Paulo Henrique de Lima Santana
short_description: |
  A Comunidade Curitiba Livre convida a todos para participarem de um encontro no próximo dia 10 de setembro na Aldeia Coworking das 19:00h às 21h30min.
---

A Comunidade Curitiba Livre convida a todos para participarem de um encontro no próximo dia 10 de setembro na [Aldeia Coworking](https://aldeia.cc/) das 19:00h às 21h30min.

O evento gratuito tem por objetivo:

* Celebrar os 11 anos da Associação Software Livre.Org (ASL.Org) que é a entidade responsável pela organização do Fórum Internacional de Software Livre (FISL) e que conta com alguns de seus associados morando em Curitiba.
* Promover um "esquenta" para a 1a Semana de Software Livre de Curitiba que acontecerá de 18 a 20 de setembro na UTFPR
* Divulgar o Software Livre e a Liberdade do Conhecimento por meio das palestras abaixo.

| Horário | Titulo | Palestrante |
|---------|--------|-------------|
| 19h00min | Junte-se a Comunidade Curitiba Livre | Paulo Henrique de Lima Santana |
| 19h30min | Liberdade e autonomia na era digital: o Software Livre como um pilar para uma sociedade autônoma | Antonio Terceiro |
| 20h00min | A importância na adoção do formato aberto de documentos e o LibreOffice | Vitório Furusho |

Para confirmar a sua presença, preencha o formulário aqui.

Não será emitido certificado de participação. A ideiaé promover um networking entre os participantes.

Quem sabe rola um bolinho para comemorar :-)

Endereço da Aldeia Coworking: Av. Cândido de Abreu, 381

![Panfleto](/images/logos-encontro-aldeia.png =600x)

