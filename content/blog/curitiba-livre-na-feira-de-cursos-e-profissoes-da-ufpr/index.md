---
kind: article
created_at: 2013-08-30
title: Curitiba Livre na Feira de Cursos e Profissões da UFPR  
image_file: banner-feira-cursos-2013.png
image_alt: Feira
author: Paulo Henrique de Lima Santana
short_description: |
 A Comunidade Curitiba Livre participou da Feira de Cursos e Profissões da UFPR que aconteceu de 30 de agosto a 01 de setembro de 2013 no Centro de Eventos do Setor de Educação Profissional e Tecnológica (Sept). 
---

A **Comunidade Curitiba Livre** participou da [Feira de Cursos e Profissões da UFPR](http://www.feiradecursos.ufpr.br/) que aconteceu de 30 de agosto a 01 de setembro de 2013 no Centro de Eventos do Setor de Educação Profissional e Tecnológica (Sept).

A organização do evento cedeu um estande onde integrantes da Comunidade fizeram um trabalho de divulgação através da distribuição de panfletos explicativos mostrando o que é Software Livre, GNU/Linux, exemplos de softwares, etc, convidando a todos para visitarem o site curitibalivre.org.br e ainda dvivulgando os eventos [SFD](http://softwarelivre.org/sfd2013-curitiba), [FTSL](http://www.ftsl.org.br/) e [Latinoware](http://www.latinoware.org/).

Também tiramos dúvidas dos visitantes e conversamos bastante, o que nos deu boas surpresas como a de descobrir estudantes do ensino que usam GNU/Linux regularmente em seus computadores.

Para a realização desse trabalho foi imprescindível o apoio da [Universidade Federal do Paraná](http://www.ufpr.br/), da empresa [Ambiente Livre](http://www.ambientelivre.com.br/) e da [Associação dos Professores da UFPR](http://www.apufpr.org.br/) para a confecção dos materiais e empréstimo de equipamentos. Também tivemos a contribuição do pessoal do [Nic.br](http://www.nic.br/) que nos enviou 20 roteadores wi-fi do Projeto [SIMETBox](http://simet.nic.br/simetbox.html) que foram sorteados entre os visitantes.

E tudo isso não seria possível sem o trabalho voluntário dos membros da Comunidade Curitiba Livre que estiveram no estande. São eles:

* Adriana da Costa
* Ana Marina Lima
* Antonio Marques
* Daniel Lenharo
* Fabianne Balvedi
* Joaquim Valverde
* Joelson Gai
* Paulo de Souza Lima
* Paulo Santana
* Rodrigo Robles
* Samuel Henrique
* Silma Battezzati
