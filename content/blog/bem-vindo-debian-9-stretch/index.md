---
kind: article
created_at: 2017-07-17
title: Bem-vindo Debian 9 Stretch
image_file: banner-debian-stretch.png
image_alt: Debian Stretch
author: Paulo Henrique de Lima Santana
short_description: |
  Foi lançado o Debian 9 - Stretch.
---

Foi lançado o Debian 9 - Stretch.

![Banner Debian Stretch](/images/debian-stretch.png =600x)

