---
kind: article
created_at: 2016-02-17
title: Produtos com a logomarca Debian a venda
image_file: banner-produtos-debian.png
image_alt: Produtos Debian
author: Paulo Henrique de Lima Santana
short_description: |
  A Comunidade Curitiba Livre está iniciando a partir de hoje a venda on-line antecipada dos produtos abaixo com a logomarca do Debian:
---

A Comunidade Curitiba Livre está iniciando a partir de hoje a venda on-line antecipada dos produtos abaixo com a logomarca do [Debian](http://www.debian.org):

* Camiseta (3 modelos)
* Caneca de porcelana
* Cordão para crachá
* Adesivo de vinil


O site para a compra é: <http://loja.curitibalivre.org.br>

Neste momento será possível fazer o pagamento da compra apenas por depósito ou transferência bancária, e **a entrega dos produtos será realizada apenas durante a Mini-DebConf nos dias 05 e 06 de março de 2016 em Curitiba.** Reforçamos a informação que não enviaremos os produtos pelo Correio.

Todo o lucro obtido com as vendas será revertido para viabilizar a organização da [Mini-DebConf Curitiba 2016](http://br2016.mini.debconf.org) e de futuros eventos de Software Livre organizados pela Comunidade Curitiba Livre.

Esta é a nossa primeira experiência com venda on-line, e as pessoas envolvidas na administração do site são todas voluntárias,  por isso pedimos a sua compreensão e paciência. Desde já nos colocamos a disposição para tirar qualquer dúvida no email: <contato@br2016.mini.debconf.org>

Não perca esta oportunidade de ter produtos com a logomarca do Debian e ajudar as comunidades Debian e de Software Livre de Curitiba. Realize a sua compra o quanto antes para que tenhamos tempo suficiente para confeccionar os produtos e entregá-los na Mini-DebConf.

<http://loja.curitibalivre.org.br> 
