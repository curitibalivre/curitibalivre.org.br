---
kind: article
created_at: 2015-08-13
title: Reunião da Comunidade Curitiba Livre neste sábado 15/08 às 9h 
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião Curitiba Livre
author: Paulo Henrique de Lima Santana
short_description: |
  Neste sábado (15/08) vamos fazer mais uma reunião da Comunidade Curitiba Livre. Será a partir das 9:00h na sala 04 no prédio do SEPT - UFPR (mesmo local que acontecerá o DebianDay a tarde).
---

Neste sábado (15/08) vamos fazer mais uma reunião da Comunidade Curitiba Livre.

Será a partir das 9:00h na sala 04 no prédio do [SEPT - UFPR](http://www.sept.ufpr.br) (mesmo local que acontecerá o [Debian Day](http://softwarelivre.org/debianday2015-curitiba) a tarde).

Endereço: Rua Doutor Alcides Vieira Arcoverde, 1225 - Jardim das América

A pauta será:

* Software Freedom Day
* Circuito Curitibano de SL
* Caravana para Latinoware
* Integração com outras comunidades de Curitiba
* Outros assuntos que surgirem

Todos estão convidados, mesmo aqueles que nunca participaram de uma reunião da Comunidade.
