---
kind: article
created_at: 2017-09-05
title: Vídeos, fotos e textos da palestra do Stallman em Curitiba em 2017 
image_file: banner-stallman-2017.png
image_alt: Stallman em Curitiba
author: Paulo Henrique de Lima Santana
short_description: |
  Richard Stallman (RMS), fundador do movimento Software Livre, do Projeto GNU, e da Free Software Foundation (FSF), e um dos autores da Licença Pública Geral GNU (GNU GPL) palestrou no dia 02 de junho de 2017 no auditório Prof. Ulysses de Campos do Setor de Ciências Sociais Aplicadas da Universidade Federal do Paraná.
---

[Richard Stallman (RMS)](https://stallman.org), fundador do movimento [Software Livre](https://pt.wikipedia.org/wiki/Software_livre), do [Projeto GNU](http://gnu.org/), e da [Free Software Foundation (FSF)](http://fsf.org/), e um dos autores da Licença [Pública Geral GNU (GNU GPL)](https://www.gnu.org/licenses) palestrou no dia 02 de junho de 2017 no auditório Prof. Ulysses de Campos do Setor de Ciências Sociais Aplicadas da Universidade Federal do Paraná.

A vinda do Stallman foi organizada pela [Comunidade Curitiba Livre](http://curitibalivre.org.br/) e pelo [Centro de Estudos de Informática (CEI)](http://cei.inf.ufpr.br/), teve o patrocínio da empresa [Ambiente Livre](http://www.ambientelivre.com.br/) e os apoios do [Departamento de Informática da UFPR](http://www.inf.ufpr.br/dinf/), do [Centro de Computação Científica e Software Livre (C3SL)](https://www.c3sl.ufpr.br/), e da [Celepar](http://www.celepar.pr.gov.br/).

A organização realizou uma campanha de financiamento coletivo para viabilizar a contratação da tradução simultânea e custear outras despesas. A campanha foi um sucesso, a lista de doadores e a prestação de contas podem ser vistas no site [http://rms.curitibalivre.org.br](http://rms.curitibalivre.org.br/financiamento-coletivo.shtml)

### Vídeos da palestra - vários formatos e áudios em inglês e português (tradução simultânea):

<http://stallman-brasil.softwarelivre.org/2017/curitiba-ufpr>

### Fotos:

<https://www.flickr.com/photos/curitibalivre/sets/72157684615095686>

### Textos publicados sobre a palestra:

**UFPR: Criador do Movimento Software Livre, Richard Stallman faz palestra para auditório lotado na UFPR**

<http://www.ufpr.br/portalufpr/blog/noticias/criador-do-movimento-software-livre-richard-stallman-faz-palestra-para-auditorio-lotado-na-ufpr>

**Jornal Comunicação UFPR: Quem controla o seu computador? Confira palestra do criador do sistema GNU em Curitiba**

<http://jornalcomunicacaoufpr.com.br/quem-controla-o-seu-computador-confira-palestra-do-criador-do-sistema-gnu-em-curitiba>

**Gazeta do Povo: Todos os olhos em você**

<http://www.gazetadopovo.com.br/economia/nova-economia/todos-os-olhos-em-voce-a0cxax3k55o1oc9iqzuur74t4>

A ***Free Software Foudation*** publicou em seu site uma notícia sobre a vinda do Stallman para o Brasil e as palestras nas cidades de Belo Horizonte, Campinas, Curitiba e Brasília.

<https://www.fsf.org/blogs/rms/photo-blog-2017-may-june-campinas-curitiba-brasilia>


![Stallman em Curitiba](/images/stallman-curitiba-2017.jpg =600x)

