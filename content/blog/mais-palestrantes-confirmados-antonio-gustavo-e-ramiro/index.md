---
kind: article
created_at: 2014-07-02
title: Mais palestrantes confirmados Antonio, Gustavo e Ramiro     
image_file: banner-semana-2014.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 Veja abaixo mais três membros ativos da comunidade de Software Livre que confirmaram presença no evento.
---

 Veja abaixo mais três membros ativos da comunidade de Software Livre que confirmaram presença no evento.

* **Antônio C. C. Marques** possui graduação em Filosofia pela Pontifícia Universidade Católica do Paraná (1990). Com Especialização em Metodologia da Ciência Pela Faculdades Integradas Espiritas. Possui uma segunda Especialização em Formulação e Gestão de Politicas Publicas. Mestrado pela Universidade Federal do Paraná, UFPR, com o projeto Título: O PROJETO UM COMPUTADOR POR ALUNO UCA: REAÇÕES NA ESCOLA, PROFESSORES, ALUNOS, INSTITUCIONAL, Ano de Obtenção: 2009.Orientador: Profa. Orientadora: Dra. Gláucia Silva Brito. Tem diversas participações em eventos com enfase do uso da tecnologias da informação na Educação, bem em eventos em relação ao uso das tecnologias Livres Atualmente é ativo - Secretaria de Estado da Educação do Paraná no Colégio Estadual Leoncio Correia de Curitiba.

* **Gustavo Mattos** é Designer formado na UFPR em 2003, Pós em Desenvolvimento de Jogos para Computador e Desenvolvimento Gerencial Universidade Positivo, experiência em computação gráfica 3D/2D e software livre desde 1996 com a produção de comerciais para a tv, vídeos institucionais e  instrucionais. Produziu programas de tv veiculados na tv local utilizando software livre. Administrador do site Blender Brasil. Atualmente trabalha na EBS Sistemas, empresa do Grupo SAGE como designer multimídia.

* **Ramiro Batista da Luz** é desenvolvedor de software desde 1991, funcionário da Diretoria de Informática da Câmara Municipal de Curitiba, membro da Associação Python Brasil e integrante de diversas comunidades de software livre, mestrado em Engenharia de Software pela Universidade Tecnológica Federal do Paraná, membro do time de testes Plone.
