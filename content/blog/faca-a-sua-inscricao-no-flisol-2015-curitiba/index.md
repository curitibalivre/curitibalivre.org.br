---
kind: article
created_at: 2015-03-22
title: Faça a sua Inscrição no FLISOL 2015 - Curitiba 
image_file: banner-flisol-2015.png
image_alt: Flisol 2015
author: Adriana Cássia da Costa
short_description: |
  A galera de Curitiba e região já pode garantir a sua vaga no Festival Latino-Americano de Instalação de Software Livre - FLISOL que vai acontecerem abril.
---

A galera de Curitiba e região já pode garantir a sua vaga no **Festival Latino-Americano de Instalação de Software Livre - FLISOL** que vai acontecerem abril.

Anote na agenda, o evento será no dia **25 de abril das 09:00 às 17:00 hs** na [PUCPR](http://www.pucpr.br/). Aproveite essa oportunidade para conhecer um pouco mais sobre Software Livre e participar de palestras, oficinas e Install Fest!

Teremos atividades para iniciantes no mundo do Software Livre com o espaço "Conhecendo o Software Livre" que vai conceituar os principais aspectos do Software Livre e exibir soluções para usar em casa, no trabalho e também nas horas de diversão.

Na programação terão palestras técnicas e não técnicas, oficinas para você aprofundar os conhecimentos em assuntos específicos. Além disso, teremos uma seleção especial de vídeos sobre Software Livre.

Se você quer instalar GNU/Linux ou ferramentas livres traga o seu computador e participe do install fest!

Então, se você é leigo, iniciante ou expert em Software Livre participe do FLISOL, estamos esperando por vocês!

O FLISOL é um evento **totalmente gratuito**, faça a sua inscrição!

Para fazer a inscrição clique  AQUI.

Siga do FLISOL - Curitiba nas redes sociais

* [Diaspora](https://diaspora.softwarelivre.org/u/curitibalivre)
* [Twitter](https://twitter.com/curitibalivre)
