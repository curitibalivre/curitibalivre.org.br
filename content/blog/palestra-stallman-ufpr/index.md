---
kind: article
created_at: 2012-12-10
title: Palestra de Richard Stallman na UFPR 
image_file: banner-stallman-2012.png
image_alt: RMS
author: Paulo Henrique de Lima Santana
short_description: |
 Richard Stallman irá palestrar na UFPR no dia 13 de dezembro às 17:00h. Veja as informações completas abaixo: 
---

Richard Stallman irá palestrar na UFPR no dia 13 de dezembro às 17:00h. Veja as informações completas abaixo:

**Data**: 13 de dezembro de 2012 (quinta-feira)

**Horário**: 17:00h

**Local**: Auditório do prédio da Administração do Centro Politécnico da Universidade Federal do Paraná (1o. andar).

**Endereço**: Rua Coronel Francisco Heráclito dos Santos, 210, Jardim das Américas - Curitiba. Mapa

**Tema**: Free Software

**Abstract**: Richard Stallman will speak about the goals and philosophy of the Free Software Movement, and the status and history of the GNU operating system, which in combination with the kernel Linux is now used by tens of millions of users world-wide.

**Idioma**: inglês

**Entrada**: totalmente gratuita e não é necessário inscrição.

**Richard Matthew Stallman**, ou simplesmente "rms" (Manhattan, 16 de março de 1953) é um famoso ativista, fundador do movimento free software, do projeto GNU, e da FSF. Um aclamado programador e Hacker, seus maiores feitos incluem Emacs (e o GNU Emacs, mais tarde), o GNU Compiler Collection e o GNU Debugger. É também autor da GNU General Public License (GNU GPL ou GPL), a licença livre mais usada no mundo, que consolidou o conceito de copyleft. Desde a metade dos anos 1990, Stallman tem dedicado a maior parte de seu tempo ao ativismo político, defendendo software livre e lutando contra a patente de softwares e a expansão da lei de copyright. O tempo que ainda devota à programação é gasto no GNU Emacs. Ele se sustenta com aproximadamente a metade do que recebe por suas palestras. Em 1971, ainda calouro na Universidade Harvard - onde se graduou em Física, em 1974 - Stallman era programador do laboratório de IA do MIT e tornou-se um líder na comunidade hacker.

Para receber informações de eventos futuros, preencha o formulário abaixo:

<https://crm.fsf.org/civicrm/profile/create?gid=155&reset=1>

Apoio:

* [Solidarius Internacional](http://www.solidarius.net/)
* [Departamento de Informática da UFPR](http://www.inf.ufpr.br/)
* [Associação dos Professores da UFPR](http://www.apufpr.org.br/)

![Stallman](/images/richardstallman1.jpg)
 
