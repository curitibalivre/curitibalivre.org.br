---
kind: article
created_at: 2014-07-11
title: Próxima reunião da comunidade 05/07/14    
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 Neste sábado, dia 5 de julho de 2014, das 9h às 12h, iremos nos reunir na sala D14 do Bloco Azul da PUCPR. Esta sala fica no térreo, ao lado da secretaria da Escola de Arquitetura e Design.
---

Por Fabianne Balvedi.

Neste sábado, dia 5 de julho de 2014, das 9h às 12h, iremos nos reunir na sala D14 do Bloco Azul da PUCPR. Esta sala fica no térreo, ao lado da secretaria da Escola de Arquitetura e Design.

A pauta do dia versará sobre quem somos enquanto comunidade, tendo como base este artigo do David de Ugarte:

<http://www.estudiolivre.org/tiki-view_blog_post.php?postId=825>

Sinopse: interação não é o mesmo que participação, e participação não é o mesmo que adesão. Se colocarmos estes conceitos como sinônimos, reduzimos a nada ou quase nada o valor do compromisso entre nós.

Também vamos definir uma equipe para decidir de uma vez por todas as questões sobre nosso logo. Quem quiser fazer parte desse grupo de trabalho deve se apresentar agora e interagir no desenvolvimento dos tramites seguintes. Vale ressaltar que o logo não será apresentado posteriormente para votação ou concurso ou qualquer coisa parecida. A equipe definida na reunião terá a confiança da comunidade para finalizar este trabalho, portanto quem quer ter voz ativa nesta decisão, tem de fazer parte desta equipe.

