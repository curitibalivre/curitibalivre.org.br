---
kind: article
created_at: 2013-06-15
title: Encontro presencial 15/06/2013  
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 Neste sábado (15/06/2013) a partir de 14:00h nos reuniremos na sede da Bicicletaria Cultural para conversar sobre as próximas ações do grupo Curitiba Livre. 
---

Neste sábado (15/06/2013) a partir de 14:00h nos reuniremos na sede da Bicicletaria Cultural para conversar sobre as próximas ações do grupo Curitiba Livre.

Endereço: Rua Presidente Faria, 226 - Centro - Curitiba PR

Venha participar e ajudar a contribuir com a comunidade de Software Livre de Curitiba.
