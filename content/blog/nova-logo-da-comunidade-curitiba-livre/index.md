---
kind: article
created_at: 2015-01-15
title: Nova logo da Comunidade Curitiba Livre  
image_file: banner-nova-logo.png
image_alt: Logo
author: Paulo Henrique de Lima Santana
short_description: |
  A Comunidade Curitiba Livre vai começar 2015 mais colorida e alegre.
---

A **Comunidade Curitiba Livre** vai começar 2015 mais colorida e alegre.

Isso mesmo! Depois de alguns meses de conversas, estudos e experimentos, a nossa logo está pronta.

A identidade visual possui elementos que remetem a cidade como a famosa calçada com petit-pavé que é formada por rochas de basalto (escuras) e rochas calcáreas (brancas) de pequeno porte formando gravuras como a do pinhão. A inclusão das cores azul e verde que adicionam os conceitos de harmonia e liberdade, sendo este último um dos aspectos importantes para o Software Livre.

O nosso **agradecimento especial** vai para a **Katia Kaori** pela pesquisa e elaboração da identidade visual e ao **Gustavo Mattos** responsável pelo estudo de cores e adaptações.

![Logo](/images/logo-curitiba-livre.png)
