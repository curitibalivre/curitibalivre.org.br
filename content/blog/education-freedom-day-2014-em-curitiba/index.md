---
kind: article
created_at: 2014-03-04
title: Education Freedom Day 2014 em Curitiba   
image_file: banner-efd-2014.png
image_alt: EFD
author: Paulo Henrique de Lima Santana
short_description: |
 A Comunidade Curitiba Livre e a Secretaria Municipal de Educação de Curitiba convidam a todos para participaram do Education Freedom Day (EFD) 2014.
---

A Comunidade Curitiba Livre e a Secretaria Municipal de Educação de Curitiba convidam a todos para participaram do [Education Freedom Day (EFD)](http://softwarelivre.org/efd2014-curitiba) 2014 que acontecerá no dia 08 de março a partir das 8h30min no Campus central da Universidade Tecnológica Federal de Curitiba (UTFPR).

O Education Freedom Day ou em português "Dia da Liberdade na Educação" é uma celebração mundial dos recursos educacionais livres.

Iniciada em 2013 pela mesma organização responsável pelo Software Freedom Day, o EFD visa educar o público em todo o mundo sobre os benefícios do uso de Software Livre e Recursos Educacionais Abertos na educação.

A programação de palestras já está disponível em nosso site. Mais informações e inscrição (gratuita) em <http://softwarelivre.org/efd2014-curitiba>.
