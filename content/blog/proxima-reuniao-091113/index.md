---
kind: article
created_at: 2013-10-29
title: Próxima reunião 09/11/13   
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 A próxima reunião presencial está marcada para o dia 09 de novembro de 2013 às 15:00h 9:00h no auditório do Fórum Ambientalista do Paraná.
---

A próxima reunião presencial está marcada para o dia 09 de novembro de 2013 às 15:00h 9:00h no auditório do Fórum Ambientalista do Paraná.

Endereço: Rua Gaspar Carrilho Júnior, 001 - Jardim Schaffer

Vista Alegre/Bom Retiro (anexo ao Bosque Gutierrez)
