---
kind: article
created_at: 2016-05-23
title: 8ª etapa do Circuito Curitibano de Software Livre - 31 de maio e 01 de junho na Opet 
image_file: banner-circuito-8-opet.png
image_alt: Circuito 8 Opet
author: Paulo Henrique de Lima Santana
short_description: |
  A 8ª etapa do Circuito Curitibano de Software Livre acontecerá durante a Semana Acadêmica da Opet, nos dias 31 de maio e 01 de junho das 19h30min às 22h30min.
---

A **8ª etapa do Circuito Curitibano de Software Livre** acontecerá durante a Semana Acadêmica da [Opet](http://www.opet.com.br/), nos dias 31 de maio e 01 de junho das 19h30min às 22h30min.

Serão 6 palestras apresentadas por membros da comunidade de Software Livre de Curitiba sobre os conceitos de [Software Livre e Distribuições GNU/Linux](http://www.gnu.org/), a linguagem de programação [Python](https://www.python.org/), como ser um SysAdmin que trabalha com GNU/Linux , o software de modelagem 3D [Blender](https://www.blender.org/), o CMS [WordPress](https://wordpress.org/), e as atividades desenvolvidade pela [Comunidade Curitiba Livre](http://www.curitibalivre.org.br) e como se juntar ao grupo.

O **Circuito Curitibano de Software Livre** é totalmente gratuito e nesta 8ª etapa é exclusivo para estudantes, professores e funcionários da Opet.

Aqueles que quiserem participar do sorteio dos brindes, deverão estar presentes no local no momento do encerramento do evento. Nesta etapa, contamos com o apoio do Nic.br que doou um roteador wi-fi do projeto [SIMETBox](http://simet.nic.br/simetbox.html) para sorteio. Também teremos algumas camisetas e outros brindes.

Veja a [programação completa](http://softwarelivre.org/circuito-curitibano/programacao-8a-etapa).

Para saber mais sobre o projeto Circuito Curitibano de Software Livre leia a nossa [apresentação](http://softwarelivre.org/circuito-curitibano/apresentacao) e se tiver interesse em levar o evento para a sua Faculdade/Universidade, entre em contato conosco através do email: <contato@curitibalivre.org.br>

![Circuito 8 Opet](/images/logos-circuito-8.png)

