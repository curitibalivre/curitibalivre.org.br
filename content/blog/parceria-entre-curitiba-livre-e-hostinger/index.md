---
kind: article
created_at: 2018-03-21
title: Parceria entre Curitiba Livre e Hostinger
image_file: banner-hostinger.png
image_alt: Hostinger
author: Paulo Henrique de Lima Santana
short_description: |
  É com grande prazer que a Comunidade Curitiba Livre oficializa parceria com a multinacional Hostinger Hospedagem de Sites.
---

É com grande prazer que a Comunidade Curitiba Livre oficializa parceria com a multinacional [Hostinger Hospedagem de Sites](https://www.hostinger.com.br).

Com mais de 29 milhões de usuários em 178 países, a **Hostinger** deu o seu pontapé inicial em 2004 e, desde então, segue em uma jornada fantástica de crescimento.

A Hostinger tem 3 anos de atuação no mercado brasileiro de tecnologia em hospedagem de sites, apresentando crescimento expressivo. Em 2017, quando começaram a estruturar os setores, equipes e processos, a empresa apresentou um crescimento de 155% de faturamento comparado ao período anterior. Eles acreditam fortemente que o potencial é de crescer muito mais nos próximos períodos!

A Hostinger fornece tecnologia em hospedagem de sites através de planos variados, para sites de tamanhos diferentes, servidores dedicados, registro de domínios e planos especiais para revenda de hospedagem. Isto tudo é acompanhado por material informativo, tutoriais, newsletters, vídeos e uma equipe de suporte de pronto atendimento 24/7. Além disso, eles oferecem migração de sites gratuita  possibilitam não só o início de novos projetos por pessoas sem experiência em hospedagem como o crescimento e evolução destes projetos em empreendimentos mais complexos. Uma plataforma completa de empoderamento de negócios online, de pequenas até grandes empresas, é algo que só a Hostinger oferece por valores tão acessíveis.

Com esta parceria, a Comunidade Curitiba Livre receberá acesso a uma [VPS](https://www.hostinger.com.br/servidor-vps) gratuitamente para hospedar serviços e ferramentas de software livre que ajudarão em nossos trabalhos.
