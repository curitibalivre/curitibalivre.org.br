---
kind: article
created_at: 2015-10-05
title: Relato e fotos do encontro em Curitiba que celebrou os 30 anos da FSF 
image_file: banner-relato-fsf30-curitiba.png
image_alt: FSF 30 anos Curitiba
author: Paulo Henrique de Lima Santana
short_description: |
  No dia 03 de outubro de 2015 a Comunidade Curitiba Livre realizou um encontro na Aldeia Coworking para celebrar os 30 anos da Free Software Foudation. A FSF convocou as comunidades locais de software livre em todo o mundo para organizarem festas nas suas cidades e a Comunidade Curitiba Livre não poderia ficar de fora dessa.
---

No dia 03 de outubro de 2015 a Comunidade Curitiba Livre realizou um encontro na [Aldeia Coworking](http://aldeiaco.com.br) para celebrar os [30 anos da Free Software Foudation](https://www.fsf.org/fsf30). A FSF convocou as comunidades locais de software livre em todo o mundo para organizarem festas nas suas cidades e a Comunidade Curitiba Livre não poderia ficar de fora dessa. É possível ver os locais que organizaram atividades [aqui](https://libreplanet.org/wiki/FSF30_Party_Network).

O encontro aconteceu das 13:00h às 18:00h e tivemos aproximadamente 35 pessoas. A principal atividade realizada foram as apresentações das comunidades de software livre de Curiitba. Tivemos as participações das comunidades abaixo que foram apresentadas pelos respectivos representantes:

* [GURU-PR (Grupo de Usuários de Ruby do Paraná)](http://www.gurupr.org) - Leonardo Saraiva
* [PHP Paraná](http://www.phppr.net) - André Cardoso
* [WordPress Curitiba](http://www.meetup.com/wpcuritiba/?_locale=pt) - Daniel Kossmann
* [PyLadies](https://www.facebook.com/PyLadiesCuritiba) -  Vanessa Romankiv
* [GruPy-PR (Grupo de Usuário de Python do Paraná)](https://github.com/GruPyPR) - Ellison Leão
* [Garagem Hacker](http://garagemhacker.org) - Igor
* Comunidade Curitiba Livre - Paulo Henrique de Lima Santana

As comunidades apresentaram:

* Os trabalhos que elas fazem;
* Como é possivel se juntar a cada grupo e passar a contribuir;
* As atividades que elas realizam como [encontros e eventos](http://curitibalivre.org.br/eventos-passados);
* Foram citadas as dificuldades que cada grupo enfrenta, principalmente pela falta de participantes em alguns desses encontros.

A troca de experiências foi bastante positiva e pudemos aprender com os erros e acertos de cada comunidade. Nos comprometemos a divulgar sempre as atividades dos outros grupos para que mais pessoas participem. E se tudo der certo, gostaríamos de realizar um evento de software livre em Curitiba com a participação de todas as comunidades na organização, dividindo as responsabilidades principalmente na elaboração da programação e na divulgação.

Tínhamos programado [mais algumas atividades](http://fsf30.curitibalivre.org.br/), mas o papo sobre as experiencias das comunidades foi tão legal que tomou praticamente todo o horário que havíamos reservado. No final, todos participaram de uma confraternização com direito a bolo, docinhos, salgados e refrigerantes :-)

Muito obrigado a todos que participaram!

Fotos do encontro: <https://www.flickr.com/photos/curitibalivre/sets/72157659422581136>

![Foto encontro](/images/foto-encontro-fsf30-1.jpg =600x)
