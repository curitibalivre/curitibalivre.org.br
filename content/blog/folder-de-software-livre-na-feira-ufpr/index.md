---
kind: article
created_at: 2013-08-31
title: Folder de Software Livre na Feira UFPR  
image_file: banner-folder-2013.png
image_alt: Folder
author: Paulo Henrique de Lima Santana
short_description: |
 O folder de Software Livre que estamos distribuindo na Feira de Cursos e Profissões da UFPR 2013. 
---

O folder de Software Livre que estamos distribuindo na Feira de Cursos e Profissões da UFPR 2013.

![Foto](/images/folder1.jpg =600x)

![Foto](/images/folder2.jpg =600x)

![Foto](/images/folder3.jpg =600x)


