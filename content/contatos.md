# Contatos

E-mail: <contato@curitibalivre.org.br>

Lista de discussão: [curitibalivre@listas.softwarelivre.org](http://listas.softwarelivre.org/cgi-bin/mailman/listinfo/curitibalivre)

Telegram: grupo [Software Livre em Curitiba](https://telegram.me/softwarelivre_curitiba)

IRC: canal #curitibalivre na rede Freenode.

## Redes sociais

 * Perfil no Twitter: [@curitibalivre](https://twitter.com/curitibalivre)
 * Grupo no Facebook: <https://www.facebook.com/groups/curitibalivre.grupo>
 * Página no Facebook: <https://www.facebook.com/CuritibaLivre>
 * Página no Meetup: <https://www.meetup.com/pt-BR/Comunidade-Curitiba-Livre>
 * Página no Get Together: <https://gettogether.community/curitiba-livre>
<!-- * Perfil no Diaspora: [cuddritibalivre](https://diaspora.softwarelivre.org/u/curitibalivre)
 * Perfil no GNU social: [@curitibalivre](https://quitter.se/curitibalivre)
-->
