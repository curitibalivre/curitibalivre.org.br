# Comunidades e Grupos de Curitiba e Região

## Software livre

Debian Curitiba

 * Site: <https://wiki.debian.org/Brasil/GUD/Curitiba>
 * Lista: <https://alioth-lists.debian.net/cgi-bin/mailman/listinfo/debian-br-gud-curitiba>
 * Endereço: <debian-br-gud-curitiba@alioth-lists.debian.net>
 * Twitter: [@DebianCuritiba](https://twitter.com/DebianCuritiba)
 * Grupo no telegram: [debiancwb](https://t.me/debiancwb)

DOJO-Paraná

 * Lista: <https://groups.google.com/forum/#!forum/dojo-parana>
 * Endereço: <dojo-parana@googlegroups.com>

Garagem Hacker

 * Site: <http://garagemhacker.org>
 * Wiki: <http://hackerspaces.org/wiki/garagemhacker>
 * Lista: <https://groups.google.com/forum/#!forum/garagemhacker>
 * Endereço: <garagemhacker@googlegroups.com>
 * Página no facebook: <https://www.facebook.com/GaragemHacker>
 * Twitter: [@garagemhacker](https://twitter.com/garagemhacker)

GUD-BR-PR - Grupo de Usuário Debian do Paraná (desativado)

 * Site antigo: https://wiki.debian.org/Brasil/GUD/PR

GURU-PR - Grupo de Usuários de Ruby do Paraná

 * Site: <http://www.gurupr.org>
 * Lista: <https://groups.google.com/forum/#!forum/guru_pr>
 * Endereço: <guru_pr@googlegroups.com>
 * Grupo no facebook: <https://www.facebook.com/groups/704256192948428>

GruPy-PR - Grupo de Usuário de Python do Paraná

 * Site: <https://grupypr.github.io>
 * Github: <https://github.com/GruPyPR>
 * Mobilize: <https://grupypr.mobilize.io>
 * Meetup: <http://www.meetup.com/pt/GruPy-PR>
 * Lista: <https://groups.google.com/forum/#!forum/grupy-pr>
 * Endereço: <grupy-pr@googlegroups.com>
 * Grupo no telegram: [grupy_pr](https://t.me/grupy_pr)

PHP Curitiba

 * Lista: <https://groups.google.com/forum/#!forum/php-curitiba>
 * Endereço: <php-curitiba@googlegroups.com>

PHPPR - Grupo de desenvolvedores de PHP do estado do Paraná 

 * Site: <https://phppr.org>
 * Meetup: <http://www.meetup.com/pt/PHP-PR>
 * Grupo no Facebook: <https://www.facebook.com/groups/1398320767076609>
 * Lista: <https://groups.google.com/forum/#!forum/phppr>

PostgreSQL Curitiba

 * Meetup: <https://www.meetup.com/pt-BR/PostgreSQL-Curitiba>

PyLadies Curitiba

 * Lista: <https://groups.google.com/forum/#!forum/pyladies-curitiba>
 * Meetup: <http://www.meetup.com/pt/PyLadies-Curitiba>
 * Página no facebook: <https://www.facebook.com/PyLadiesCuritiba>

Comunidade WordPress Curitiba

 * Meetup: <http://www.meetup.com/wpcuritiba>
 * Página no Facebook: <https://www.facebook.com/ComunidadeWordPressCuritiba>

## Outros
 
Code for Curitiba

 * Site: <http://www.codeforamerica.org/brigade/Code-for-Curitiba>
 * Github: <https://github.com/CodeForCuritiba>
 * Meetup: <http://www.meetup.com/pt/OpenBrazil>
 * Lista: <https://groups.google.com/forum/#!forum/openbrazil>
 * Endereço: <openbrazil@googlegroups.com>
 * Página no facebook: <https://www.facebook.com/Code-For-Curitiba-728703680591258>

Data Science Curitiba

 * Meetup: <https://www.meetup.com/pt-BR/Data-Science-Curitiba>

Elixir |> CWB

 * Meetup: <https://www.meetup.com/pt-BR/elixircwb>

Free Code Camp Curitiba

 * Meetup: <http://www.meetup.com/pt/Free-Code-Camp-Curitiba>

Google Business Group (GBG) Curitiba

 * Site: <http://www.gbgcuritiba.org>
 * Facebook: <https://www.facebook.com/gbgcuritiba>
 * Twitter: [@gbgcuritiba](https://twitter.com/gbgcuritiba)
 * Página no Google+: <https://plus.google.com/+gbgCuritibaOrg>
 * Comunidade no Google+: <https://plus.google.com/communities/110156682105851963397>

Google Developer Group (GDG) Curitiba

 * Lista: <https://groups.google.com/forum/#!forum/gdgcuritiba>
 * Endereço: <gdgcuritiba@googlegroups.com>
 * Página no Google+: <https://plus.google.com/+GdgcuritibaBrasil/posts>

React CWB

 * Meetup: <https://www.meetup.com/pt-BR/ReactJS-CWB>

Women Techmakers Curitiba

 * Página no Facebook: <https://www.facebook.com/Women-Techmakers-Curitiba-886440344765453>

